#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Cr�ation de la 1�re structure concernant le fonctionnement du jeu
struct JeuGlobal
{
    int combinaisonADeviner;
    unsigned int nbrEssaiRestants;
    int nbrPartiesGagnes;
    int nbrPartiesPerdues;
};

//Cr�ation de la 2�me structure concernant le test
struct PlacementChiffre
{
    int chiffreBienPlaces;
    int chiffreMalPlaces;
};

int main()
{
    // Initialisation nombre parties gagn�s
    struct JeuGlobal partiesgagnes;
    partiesgagnes.nbrPartiesGagnes = 0;

    // Initialisation nombre parties perdues
    struct JeuGlobal partiesperdues;
    partiesperdues.nbrPartiesPerdues = 0;

    int rejoue;
    do
    {

        //D�finition des variables
        srand(time(NULL));
        int i;
        int resultatJoueur[4];
        int rejoue;

        //Ordinateur genere la combinaison secrete
        struct JeuGlobal devinette[4];
        devinette[1].combinaisonADeviner = rand()%10;
        devinette[2].combinaisonADeviner = rand()%10;
        devinette[3].combinaisonADeviner = rand()%10;
        devinette[4].combinaisonADeviner = rand()%10;

        // Initialisation nombre d'essai restant � 10
        struct JeuGlobal essairestant;
        essairestant.nbrEssaiRestants = 10;

        // Initialisation des chiffres qui sont bien places et des chiffres qui sont mal places
        struct PlacementChiffre bienplaces;
        bienplaces.chiffreBienPlaces = 0;
        struct PlacementChiffre malplaces;
        malplaces.chiffreMalPlaces = 0;



        // Il faut boucler ici : tant que le joueur a des essais et que la partie n'est pas gagner il joue
            do
            {

                // Le joueur entre les 4 chiffres de la combinaison en mode successif
                for (i=0;i<4;i++)
                {
                printf("\nVeuillez entrer un chiffre entre 0 et 9\n");
                scanf("%d",&resultatJoueur[i]);
                }

                // On affiche les choix du joueur et de l'ordinateur
                printf("Votre combinaison est %d, %d, %d, %d\n", resultatJoueur[0],resultatJoueur[1],resultatJoueur[2],resultatJoueur[3]);
                printf("La combinaison de l'ordi est %d, %d, %d, %d\n", devinette[1].combinaisonADeviner,devinette[2].combinaisonADeviner,devinette[3].combinaisonADeviner,devinette[4].combinaisonADeviner);

                // Test de la combinaison du joueur

                for (i=0;i<4;i++)
                {

                    // On teste si le chiffre indice 1 du joueur est �gal au chiffre indice 1 de l'ordi
                    if (resultatJoueur[i] == devinette[i].combinaisonADeviner)
                    {
                        printf("Votre %deme chiffre est egal au %deme chiffre de l'ordinateur\n",i,i);
                        bienplaces.chiffreBienPlaces ++;
                    }
                    else
                    {
                        printf("Votre %deme chiffre n'est pas egal au %deme chiffre de l'ordinateur\n",i,i);
                    }

                    // On teste si le chiffre indice 1 du joueur correspond � un chiffre de l'ordinateur

                    if ((resultatJoueur[i] == devinette[1].combinaisonADeviner) || (resultatJoueur[i] == devinette[2].combinaisonADeviner) || (resultatJoueur[i] == devinette[3].combinaisonADeviner) || (resultatJoueur[i] == devinette[4].combinaisonADeviner))
                    {
                        printf("Votre %deme chiffre est egal a l'un des chiffres de l'ordinateur\n",i);
                        malplaces.chiffreMalPlaces ++;
                    }
                    else
                    {
                        printf("Votre %deme chiffre n'est pas egal a l'un des chiffres de l'ordinateur\n",i);
                    }
                    // Renvoie le nombre de chiffre bien plac�s
                    printf("Vous avez %d chiffres bien places\n",bienplaces.chiffreBienPlaces);

                    // Renvoie le nombre de chiffre mal plac�s
                    printf("Vous avez %d chiffres mal places\n",malplaces.chiffreMalPlaces);

                }

                // Je d�cremante les essais restants en partant du nombre de base qui est 10
                essairestant.nbrEssaiRestants --;
                printf("Il vous reste %d essai restants",essairestant.nbrEssaiRestants);
            } while ((essairestant.nbrEssaiRestants > 0) && (bienplaces.chiffreBienPlaces != 4));

        // Le joueur vient de perdre, on incr�mante le nombre de parties perdus
        partiesperdues.nbrPartiesPerdues ++;

        // On informe le joueur du nombre de partie gagn� et perdus

        printf("Vous avez gagne %d parties",partiesgagnes.nbrPartiesGagnes);
        printf("Vous avez perdus %d parties", partiesperdues.nbrPartiesPerdues);

        // On demande au joueur si il souhaite rejouer
        printf("Souhaitez-vous recommencer une nouvelle partie ? (0) pour oui, (1) pour non:\n");
        scanf("%d",&rejoue);
    } while(&rejoue == 0);
    return 0;
}
